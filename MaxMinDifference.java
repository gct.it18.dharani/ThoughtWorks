import java.util.Scanner;

public class MaxMinDifference {
    public  static  void  main(String[] args){
        Scanner sc = new Scanner(System.in);
        int numOfElements = sc.nextInt();
        int[] array = new int[numOfElements];
        for (int i = 0; i < numOfElements; i++){
            array[i] = sc.nextInt();
        }
        System.out.println(Difference(array));
    }

    private static int Difference(int[] array) {
        int min = array[0];
        int max = array[0];
        for(int i=1; i<array.length; i++){
            if( min > array[i] ) min = array[i];
            if( max < array[i] ) max = array[i];
        }
        return max - min ;
    }
}

 /*   Input
        5  
        1
        22
        100
        34
        55
        
      Output
        99
  */
      