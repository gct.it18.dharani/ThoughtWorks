import java.util.Scanner;

public class WeeklySalary {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] workedHrsPerDay = new int[7];
        for(int i=0;i<workedHrsPerDay.length;i++){
            workedHrsPerDay[i] = sc.nextInt();
        }
        System.out.println(Salary(workedHrsPerDay));
    }

    private static int Salary(int[] workedHrsPerDay) {
        int totalSalary = 0 ;

        // Adding Sunday salary
        int sunday = workedHrsPerDay[0] * 100;
        totalSalary += sunday + (sunday * .50);

        // Adding Monday to Friday salary
        int monToFriHrs = 0;
        for(int i=1;i< 7-1;i++){
            monToFriHrs += (workedHrsPerDay[i]*100) + (workedHrsPerDay[i] > 8 ? (workedHrsPerDay[i] - 8)*15 : 0 );
        }
        totalSalary += monToFriHrs ;

        // Adding Saturday salary
        int saturday = workedHrsPerDay[6] * 100;
        totalSalary += saturday + ( saturday * .25 );

        return  totalSalary;
    }
}

/*
Input
0
8
8
8
10
6
0

Output 
4030
 */
