import java.util.Scanner;

public class MangoTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int column = sc.nextInt();
        int treeNumber = sc.nextInt();
        boolean mangoTree = false ;

        if( 1 <= treeNumber && treeNumber <= column ){
            mangoTree = true;
        }
        else {
            int firstColumnNumber = 1+column;
            int lastColumnNumber = column+column;
            for(int i= 1 ; i < row;i++){
                if( firstColumnNumber == treeNumber || lastColumnNumber == treeNumber){
                    mangoTree = true ;
                    break;
                }
                else {
                    firstColumnNumber += column;
                    lastColumnNumber += column;
                }
            }
        }
        System.out.println( ( mangoTree ? "Yes" : "No"));
    }
}
