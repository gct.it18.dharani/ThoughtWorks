import java.util.Scanner;

public class MagicSquare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[][] squareGrid = new int[num][num];
        for(int i=0;i<num;i++){
            for(int j=0;j<num;j++){
                squareGrid[i][j] = sc.nextInt();
            }
        }
        System.out.println(CheckMagicSquare(squareGrid,num));


    }

    private static String CheckMagicSquare(int[][] squareGrid, int num) {
        int diagonal1 = 0,diagonal2=0;
        for (int i = 0; i < num; i++){
            diagonal1 += squareGrid[i][i];
            diagonal2 += squareGrid[i][num-1-i];
        }
        if( diagonal1 != diagonal2)
            return "No";

       for (int i = 0; i < num ; i++) {

            int rowSum = 0, colSum = 0;
            for (int j = 0; j < num; j++)
            {
                rowSum += squareGrid[i][j];
                colSum += squareGrid[j][i];
            }
            if (rowSum != colSum || colSum != diagonal1)
                return "No";
        }
        return "Yes";
    }
}
