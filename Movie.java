
import java.util.*;

public class Movie {

    private String title;
    private String studio;
    private String rating;

    Movie(){ }

    Movie(String title, String studio, String  rating){
        this.title = title;
        this.studio = studio;
        this.rating = rating;
    }

    Movie(String title, String studio){
        this.title = title;
        this.studio = studio;
        this.rating = "PG";
    }
    public ArrayList<Movie> getPg(ArrayList<Movie> movie){
        ArrayList<Movie> pgMovies = new ArrayList<>();
        for (Movie m: movie){
            if( m.rating.equals("PG")){
                pgMovies.add(m);
            }
        }
        return pgMovies;
    }

    public String toString(){
        return  " Title: " + this.title + " Studio: " + this.studio + " Rating: " + this.rating + " " ;
    }

    public static void main(String[] args) {
        Movie movie1 = new Movie("Casino Royale", "Eon Productions", "PG-13");

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        movies.add(new Movie("Mary Poppins", "Walt Disney Productions"));
        movies.add(new Movie("Boss Baby", "Walt Disney Productions"));
        movies.add(new Movie("Harry Potter and the Chamber of Secrets ","Warner Bros. Pictures"));

        Movie movie = new Movie();
        System.out.println(movie.getPg(movies));
        
    }
}
