import java.util.Scanner;

public class ArrayType {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arrayElements = new int[num];

        for (int i = 0; i < num; i++) {
            arrayElements[i] = sc.nextInt();
        }
        int type = typeOfArray(arrayElements, num);
        System.out.println( type == 1 ? "Odd" : (type == 2 ? "Even" : "Mixed"));
    }

    private static int typeOfArray(int[] arrayElements, int num) {
        int evenNumbersCount = 0, oddNumbersCount = 0;
        for(int i: arrayElements){
            if( i%2== 0 ) ++evenNumbersCount;
            else ++oddNumbersCount;
        }
        return (evenNumbersCount == num ? 1 : (oddNumbersCount == num ? 2 : 3 ));
    }
}

 /* Input
        5
        55
        44
        35
        22
        15

    Output    
        Mixed */