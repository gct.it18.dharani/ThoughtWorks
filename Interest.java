public class Interest {
    private String name;
    private int amount;
    private int numberOfYears ;
    private int rateOfInterest;
    private String typeOfAccount;
    Interest(String name, int amount, int numberOfYears, int rateOfInterest, String typeOfAccount){
        this.name = name;
        this.amount = amount;
        this.numberOfYears = numberOfYears;
        this.rateOfInterest = rateOfInterest;
        this.typeOfAccount = typeOfAccount;
    }


    public static void main(String[] args) {
        Interest customer1 = new Interest("Amitra", 100000,3/12, 3,"Savings");
        System.out.println(customer1.simpleInterest());
        Interest customer2 = new Interest("Gopal", 60000, 3/12,0, "Current" );

    }

    private double simpleInterest() {
        return (this.amount* this.numberOfYears * this.rateOfInterest)/100;
    }
}
