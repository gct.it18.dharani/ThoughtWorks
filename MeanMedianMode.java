import java.lang.*;
import java.util.*;

public class MeanMedianMode {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] array = new int[num];
        for(int i=0;i<num;i++){
            array[i] = sc.nextInt();
        }
        System.out.println( " Mean " + Mean(array, num));
        System.out.println( " Median " + Median(array,num));
         System.out.println( " Mode " + Mode(array,num));
    }

    private static double Mean(int[] array, int num) {
        int sum = 0 ;
        for (int i:array){
            sum += i;
        }
        return  (double)sum/num;
    }

    private static double Median(int[] array, int num) {
        Arrays.sort(array);
        if (num % 2 != 0)
            return array[num / 2];

        return (array[(num - 1) / 2] + array[num / 2]) / 2.0;
    }

    private static double Mode(int[] array, int num) {

        int maxValue=0;
        int maxFreq =0;
        for(int i=0; i< num; ++i)
        {
            int count=0;
            for(int j=0; j<num; ++j)
            {
                if(array[j] == array[i])
                {
                    ++count;
                }

                if(count > maxFreq)
                {
                    maxFreq = count;
                    maxValue = array[i];
                }
            }
        }
        return  maxValue;
    }
}
